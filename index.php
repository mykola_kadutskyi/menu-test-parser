<?php
require_once( 'lib/Autoloader.php' );

$configs = include( 'config.php' );


if ( ! empty( $_FILES['word_document'] ) ) {

	//Uploads doc/docx file
	$upload = Upload::factory( $configs['upload_dir'] );
	$upload->file( $_FILES['word_document'] );
	//set max. file size (in mb)
	$upload->set_max_file_size( 10 );
	//set allowed mime types
	$upload->set_allowed_mime_types( array(
		'application/msword',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/zip'
	) );
	$results = $upload->upload();

	//Converts document into text
	$coversion = new Docx_Conversion( $results['path'] );
	$converted = $coversion->convertToText();
	file_put_contents( $results['path'] . ".txt", $converted );

	//Parsing
	$topars = new Menu_Test_Doc_Parser( $results['filename'] . ".txt" );
	$topars->initiate_parsing();
	if ( $topars->groups ) {
		foreach ( $topars->get_result() as $key => $item ) {
			echo "<p style='color: darkred'>Group name: " . $key . " </p>";
			foreach ( $item as $value ) {
				echo "<hr>";
				echo " <span style='color: aqua'>question type: </span>" . $value['type'] . "<br>";
				echo "<span style='color: aqua'>question text: </span>" . $value['question'] . "<br>";
				if ( is_string( $value['answer'] ) ) {
					echo "<span style='color: aqua'>Answer: </span>".$value['answer'];
				} elseif ( is_array( $value['answer'] ) ) {
					echo "<span style='color: aqua'>Answer: </span>";
					echo "<pre>";
					print_r( $value['answer'] );
					echo "</pre>";
				} elseif ( is_bool( $value['answer'] ) ) {
					if ( $value['answer'] ) {
						echo "<span style='color: aqua'>Answer: </span>"."True";
					} else {
						echo "<span style='color: aqua'>Answer: </span>"."False";
					}
				}

				echo "<hr>";
			}
		}
	} else {
		foreach ( $topars->get_result() as $value ) {
			echo "<hr>";
			echo " <span style='color: aqua'>question type: </span>" . $value['type'] . "<br>";
			echo "<span style='color: aqua'>question text: </span>" . $value['question'] . "<br>";
			if ( is_string( $value['answer'] ) ) {
				echo "<span style='color: aqua'>Answer: </span>".$value['answer'];
			} elseif ( is_array( $value['answer'] ) ) {
				echo "<span style='color: aqua'>Answer: </span>";
				echo "<pre>";
				print_r( $value['answer'] );
				echo "</pre>";
			} elseif ( is_bool( $value['answer'] ) ) {
				if ( $value['answer'] ) {
					echo "<span style='color: aqua'>Answer: </span>"."True";
				} else {
					echo "<span style='color: aqua'>Answer: </span>"."False";
				}
			}

			echo "<hr>";
		}
	}
}

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Menu Test Parser</title>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
	<input type="file" name="word_document" id="word_document">
	<input type="submit" value="Upload Doc/Docx" name="submit">
</form>

</body>
</html>

<?php


?>


