<?php

/**
 * Class Autoloader
 */
class Autoloader {

	/**
	 * @param $className
	 *
	 * @return bool
	 */
	static public function loader($className) {
		$filename = "lib/class-" . strtolower(implode('-',explode('_', $className))). ".php";
        if (file_exists($filename)) {
            include($filename);
            if (class_exists($className)) {
                return TRUE;
            }
        }
        return FALSE;
    }

}

spl_autoload_register('Autoloader::loader');
