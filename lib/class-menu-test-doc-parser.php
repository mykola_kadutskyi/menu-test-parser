<?php


/**
 * Class Menu_Test_Doc_Parser
 */
class Menu_Test_Doc_Parser {


	private $prepared_array;

	private $filename;

	private $result;

	public $groups;

	private $doc_array;

	private $question_types = array(
		'true_false',
		'multi_choice',
		'open_end',
	);


	/**
	 * Menu_Test_Doc_Parser constructor.
	 *
	 * @param $filename
	 */
	public function __construct( $filename ) {
		$this->filename  = $filename;
		$this->doc_array = file( 'uploads/' . $filename, FILE_IGNORE_NEW_LINES );
	}

	/**
	 * Order of find_ methods is significant! Because each type of question have similarities between each other.
	 *
	 * @param $array
	 */
	protected function parser_manager( &$array ) {

		$this->find_multiple_choice( $array );
		$this->find_true_false( $array );
		$this->find_open_end( $array );

	}


	public function initiate_parsing() {
		if ( $this->find_logic_groups() ) {
			$groups = $this->groups;
			foreach ( $groups as &$group ) {
				$this->parser_manager( $group );
			}
			$this->prepared_array = $groups;
		} else {
			$array = $this->doc_array;
			$this->parser_manager( $array );
			$this->prepared_array = $array;
		}
		$this->prepare_result();
		//Begin of debagging
		//TODO Delet before deployment
//		echo"<pre>";
//		print_r( $this->prepared_array );
//		echo "</pre>";
//		$withtype = 0;
//		$d        = 0;
//		$arr      = $this->prepared_array;
//		$c        = count( $arr );
//		foreach($this->prepared_array as $arr){
//			$c = count($arr);
//		for ( $i = 0; $i < $c; $i ++ ) {
//			if ( is_array( $arr[ $i ] ) and ! empty( $arr[ $i ] ) and $arr[ $i ] != " " ) {
//				$withtype ++;
//				$d ++;
//			} elseif ( empty( trim( $arr[ $i ] ) ) or $arr[ $i ] == " " ) {
//
//			} else {
//				$d ++;
//			}
//		}
//		}
//		echo "<br>Parsed " . $withtype / $d * 100 . " %<br>".$withtype;
		return $this->prepared_array;
		//End of debugging
	}

	private function find_open_end( &$array ) {
		foreach ( $array as $key => &$item ) {
			if ( (array) $item === $item ) {
				if ( $item['type'] ) {
					continue;
				} else {
					return "Wrong data input";
				}

			} elseif ( is_string( $item ) ) {
				if ( ( $this->end_by_question( $item ) or $this->end_by_dot( $item ) or $this->contain_underscores( $item ) ) and ! $this->begins_with_numeric_counter( $item ) ) {

					if ( $this->end_by_question( $item ) ) {
						$item = $this->prepare_answer( 'open_end', $item );
					} elseif ( $this->end_by_dot( $item ) and $this->contain_underscores( $item ) ) {
						$item = $this->prepare_answer( 'open_end', $item );
					} elseif ( $this->contain_underscores( $item ) ) {
						$item = $this->prepare_answer( 'open_end', $item );
					}

				}
			}
		}

	}

	private function find_true_false( &$array ) {
		foreach ( $array as $key => &$item ) {

			if ( (array) $item === $item ) {
				if ( $item['type'] ) {
					continue;
				} else {
					return "Wrong data input";
				}

			} elseif ( is_string( $item ) ) {
				if ( ( $this->contain_true( $item ) or $this->contain_false( $item ) ) and ! $this->contain_underscores( $item ) and ! $this->begins_with_alphabetic_counter( $item ) ) {
					if ( $this->contain_true( $item ) and $this->contain_false( $item ) ) {
						//TODO Additional logic
						if ( strlen( trim( $item ) ) > 11 ) {
							if ( $check = $this->begins_with_numeric_counter( $item ) ) {
								$item = trim( str_replace( $check[0]['matches'][0][0][0], "", $item ) );
							}
							if ( $check = $this->what_where_matches( array( $item ), '/(?:true|false)\/(?:true|false)/i', PREG_OFFSET_CAPTURE ) ) {
								$item = $this->prepare_answer( 'true_false', trim( str_ireplace( $check[0]['matches'][0][0][0], "", $item ) ) );
							}
						}
					} elseif ( $this->contain_true( $item ) xor $this->contain_false( $item ) ) {
						if ( strlen( trim( $item ) ) <= 5 ) {
							if ( $this->what_where_matches( array( $item ), '/^(?i:true|false)$/i' ) ) {
								if ( $check = $this->begins_with_numeric_counter( $array[ $key - 1 ] ) ) {
									$array[ $key - 1 ] = trim( str_replace( $check[0]['matches'][0][0][0], "", $array[ $key - 1 ] ) );
								}
								$array[ $key - 1 ] = $this->prepare_answer( 'true_false', $array[ $key - 1 ], $item );
								unset( $array[ $key ] );
							}
						} elseif ( strlen( trim( $item ) ) > 5 ) {
							if ( $check = $this->begins_with_numeric_counter( $item ) ) {
								$item = trim( str_replace( $check[0]['matches'][0][0][0], "", $item ) );
							}
							if ( $check = $this->what_where_matches( array( $item ), '/^(?:true|false)|(?:true|false)$/i', PREG_OFFSET_CAPTURE ) ) {

								$item = $this->prepare_answer( 'true_false', trim( str_ireplace( $check[0]['matches'][0][0][0], "", $item ) ), $check[0]['matches'][0][0][0] );
							}
						}
					}
				} else {
					continue;
				}
			} else {
				return "Wrong data input in " . $key . " element: " . $item;
			}
		}
		$array = array_values( $array );
	}

	private function find_multiple_choice( &$array ) {
		foreach ( $array as $key => &$item ) {
			if ( (array) $item === $item ) {
				if ( $item['type'] ) {
					continue;
				} else {
					return "Wrong data input";
				}

			} elseif ( is_string( $item ) ) {
				if ( $check = $this->begins_with_numeric_counter( $item ) ) {
					if ( $answer = $this->have_multi_answers( $array, $key ) ) {
						$item  = $this->prepare_answer( 'multi_choice', trim( str_ireplace( $check[0]['matches'][0][0][0], "", $item ) ), $answer );
						$array = array_values( $array );
					}
				}

			}
		}
	}

	private function next_is_different_type() {

	}

	protected function prepare_result() {
		if ( $this->groups ) {
			foreach ( $this->prepared_array as $key => $group ) {
				foreach ( $group as $item ) {
					if ( (array) $item === $item ) {
						$result[$key][] = $item;
					}
				}
			}
		} else {
			foreach ( $this->prepared_array as $item ) {
				if ( (array) $item === $item ) {
					$result[] = $item;
				}
			}
		}
		$this->result = $result;
	}

	public function get_result() {
		$result = $this->result;
		return $result;
	}


	/**
	 *
	 * Searches for logic groups like WORDS: (words should be uppercase and ends by colon)
	 *
	 * @param $filename
	 *
	 * @return array|bool
	 */
	protected function find_logic_groups() {
		$arrtext = $this->doc_array;

		if ( $titles = $this->what_where_matches( $arrtext, "/[A-Z]+(?:\'[A-Z]+|):$/", PREG_OFFSET_CAPTURE ) ) {

			$groups = array();
			if ( $titles[0][0] != 0 ) {
				$groups[0] = array_slice( $arrtext, 0, $titles[0][0] );
			}
			$c = count( $titles );
			foreach ( $titles as $key => $title ) {
				if ( $key != ( $c - 1 ) ) {
					$length                          = $titles[ $key + 1 ][0] - $title[0] - 1;
					$groups[ $arrtext[ $title[0] ] ] = array_slice( $arrtext, $title[0] + 1, $length );
				} else {
					$groups[ $arrtext[ $title[0] ] ] = array_slice( $arrtext, $title[0] + 1 );
				}

			}
			$this->groups = $groups;

			return true;
		}

		return false;
	}

	/**
	 * @param array $array
	 * @param $pattern
	 * @param $flag
	 * @param int $offset
	 *
	 * @return array|string
	 */
	private function what_where_matches( array $array, $pattern, $flag = null, $offset = 0 ) {
		$matches = array();

		foreach ( $array as $key => $string ) {

			if ( $check = preg_match_all( $pattern, $string, $m, $flag, $offset ) ) {
				if ( $check === 0 ) {
					continue;
				}
				if ( false === $check ) {
					return "Bad pattern";
				}
				if ( $check > 0 ) {
					$matches[] = array( $key, 'matches' => $m );
				}
			}
		}

		return $matches;
	}

	/**
	 * @param $type | possible values in $this->question_types
	 * @param $question
	 * @param $answer
	 *
	 * @return array|string
	 */
	private function prepare_answer( $type, $question, $answer = 'empty' ) {

		if ( in_array( $type, $this->question_types, true ) ) {
			$checked_type = $type;
		} else {
			return "Wrong question type input";
		}

		if ( $checked_type == 'true_false' ) {

			if ( mb_strtolower( $answer ) == 'true' ) {
				$answer = true;
			} elseif ( mb_strtolower( $answer ) == 'false' ) {
				$answer = false;
			} else {
				$answer = 'empty';
			}
		}

		return array( 'type' => $checked_type, 'question' => $question, 'answer' => $answer );
	}

	/**
	 * @param $string
	 *
	 * @return bool
	 */
	private function end_by_question( $string ) {
		return "?" == substr( $string, - 1 );
	}

	/**
	 * @param $string
	 *
	 * @return bool
	 */
	private function end_by_dot( $string ) {
		return "." == substr( $string, - 1 );
	}

	/**
	 * @param $string
	 *
	 * @return bool
	 */
	private function end_by_colon( $string ) {
		return ":" == substr( $string, - 1 );
	}

	/**
	 * @param $string
	 *
	 * @return int
	 */
	private function contain_true( $string ) {
		if ( strripos( $string, 'true' ) !== false ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $string
	 *
	 * @return int
	 */
	private function contain_false( $string ) {
		if ( strripos( $string, 'false' ) !== false ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $string
	 *
	 * @return bool|int
	 *
	 */
	private function contain_underscores( $string ) {
		if ( strpos( $string, '__' ) !== false ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $string
	 *
	 * @return array|string
	 */
	private function begins_with_numeric_counter( $string ) {
		return $this->what_where_matches( array( $string ), '/^[0-9]+(?:\.|\))/', PREG_OFFSET_CAPTURE );
	}

	private function begins_with_alphabetic_counter( $string ) {
		return $this->what_where_matches( array( $string ), '/^[a-zA-Z](?:\.|\))/', PREG_OFFSET_CAPTURE );
	}

	/**
	 * @param $array
	 * @param $key
	 * @param bool $flag | if false - will check previous strings, if true - following strings
	 *
	 * @return int | number of strings
	 */
	private function has_empty_string_nearby( $array, $key, $flag = false ) {
		$empty = 0;
		if ( ! $flag ) {
			for ( $i = $key; $key >= 0; $i -- ) {
				if ( ! $array[ $i ] ) {
					break;
				} else {
					$empty ++;
				}
			}

		} else {
			$c = count( $array );
			for ( $i = $key; $key < $c; $i ++ ) {
				if ( ! $array[ $i ] ) {
					break;
				} else {
					$empty ++;
				}

			}
		}

		return $empty;
	}

	/**
	 * @param array $array
	 * @param $key
	 *
	 * @return array
	 */
	private function have_multi_answers( array &$array, $key ) {
		$answer = array();
		$c      = count( $array );
		for ( $i = $key + 1; $i < $c; $i ++ ) {
			if ( ! is_string( $array[ $i ] ) or empty( $array[ $i ] ) or in_array( $array[ $i ], array(
					"",
					" "
				) ) or $this->begins_with_numeric_counter( $array[ $i ] ) or $this->end_by_question( $array[ $i ] ) or $this->end_by_colon( $array[ $i ] or $this->contain_underscores( $array[ $i ] ) )
			) {
				break;

			} elseif ( is_string( $array[ $i ] ) and $check = $this->begins_with_alphabetic_counter( $array[ $i ] ) ) {
				$answer[] = trim( str_replace( $check[0]['matches'][0][0][0], "", $array[ $i ] ) );
				unset( $array[ $i ] );
			} elseif ( is_string( $array[ $i ] ) and ! $this->begins_with_numeric_counter( $array[ $i ] ) and ! $this->begins_with_alphabetic_counter( $array[ $i ] ) and $array[ $i ] != "" ) {
				$answer[] = trim( $array[ $i ] );
				unset( $array[ $i ] );
			}
		}

		return $answer;
	}
}